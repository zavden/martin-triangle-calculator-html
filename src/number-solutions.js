const pi = Math.PI

const ROUND = n => Number(n).toFixed(2)

const SOL_SSS = (SIDES, SIDES_TEX, ERRORS) => {
    let
        FIGURES = [],
        l1 = SIDES.a.side,
        l2 = SIDES.b.side,
        l3 = SIDES.c.side
    SIDES_TEX = Object.assign({}, SIDES);
    FIGURES.push(step_figure_svg(SIDES_TEX))

    console.log("==>==>==>==>==>==>==>==>")
    let sides_arr = [
        ["a", l1],
        ["b", l2],
        ["c", l3],
    ]
    let sorted_arr = sides_arr.sort(function(a, b) {
        return b[1] - a[1];
    });
    let max_letter = sorted_arr[0][0]
    let max_value = sorted_arr[0][1]
    let middle_value = sorted_arr[1][1]
    let middle_letter = sorted_arr[1][0]
    let minor_value = sorted_arr[2][1]
    let minor_letter = sorted_arr[2][0]
    let max_angle = get_arc_cos(max_value, middle_value, minor_value, ERRORS)
    let middle_angle = get_arcsin(max_angle, max_value, middle_value)
    let minor_angle = pi - max_angle - middle_angle

    console.log("Step 1 --------------")
    console.log(`max_letter = ${max_letter}`)
    console.log(`max_letter = ${max_value}`)
    console.log(`max_angle = ${max_angle * 180 / pi}`)
    SIDES_TEX[max_letter]["angle"] = Number(max_angle*180/pi).toFixed(2)
    FIGURES.push(step_figure_svg(SIDES_TEX))
    console.log("Step 2 --------------")
    console.log(`middle_letter = ${middle_letter}`)
    console.log(`middle_letter = ${middle_value}`)
    console.log(`middle_angle = ${middle_angle * 180 / pi}`)
    SIDES_TEX[middle_letter]["angle"] = Number(middle_angle*180/pi).toFixed(2)
    FIGURES.push(step_figure_svg(SIDES_TEX))
    console.log("Step 3 --------------")
    console.log(`minor_letter = ${minor_letter}`)
    console.log(`minor_letter = ${minor_value}`)
    console.log(`minor_angle = ${minor_angle * 180 / pi}`)
    console.log("<==<==<==<==<==<==<==<==")
    //
    let a1 = get_arc_cos(l1, l2, l3, ERRORS)
    //
    let a2 = get_arc_cos(l2, l1, l3, ERRORS)

    let a3 = pi - a1 - a2
    SIDES.max_values = {
        "max": {
            "side": max_value,
            "letter": max_letter,
            "angle": max_angle
        },
        "middle": {
            "side": middle_value,
            "letter": middle_letter,
            "angle": middle_angle
        },
        "minor": {
            "side": minor_value,
            "letter": minor_letter,
            "angle": minor_angle
        },
    }

    SIDES.a.angle = a1
    SIDES.b.angle = a2
    SIDES.c.angle = a3

    SIDES.tex = SOL_SSS_TEX(SIDES)
    SIDES.tex.push(FIGURES)
    return [set_results(SIDES)]
}

const SOL_SAS = (s1,s2,a,SIDES, SIDES_TEX) => {
    SIDES_TEX = JSON.parse(JSON.stringify(SIDES))
    console.log("zxxxxx")
    console.log(SIDES_TEX)
    let
        FIGURES = [],
        l1 = Number(SIDES[s1]["side"]),
        l2 = Number(SIDES[s2]["side"]),
        a3 = Number(SIDES[a]["angle"] * pi / 180),
        _a, _s, _sides = ["a","b","c"], _second, _last
    FIGURES.push(step_figure_svg(SIDES_TEX))

    let l3 = get_side_from_2_sides_and_angle(l1, l2, a3)
    SIDES[a]["side"] = Number(l3)
    SIDES_TEX[a]["side"] = ROUND(l3)
    FIGURES.push(step_figure_svg(SIDES_TEX))

    console.log("==>==>==>==>==>==>==>==>")

    console.log("Step 1 --------------")
    console.log(`a = ${a}; l3 = ${l3}`)
    console.log("Step 2 --------------")
    let t1 = l1**2 + l2**2 > l3**2,
        t2 = l2**2 + l3**2 > l1**2,
        t3 = l1**2 + l3**2 > l2**2
    console.log(`${s1}^2 + ${s2}^2 > ${a}^2 : ${t1}`)
    console.log(`${l1}^2 + ${l2}^2 > ${l3}^2 : ${t1}`)
    console.log(`${l1**2 + l2**2} > ${l3**2} : ${t1}`)
    console.log(`${s2}^2 + ${a}^2 > ${s1}^2 : ${t2}`)
    console.log(`${l2}^2 + ${l3}^2 > ${l1}^2 : ${t2}`)
    console.log(`${l2**2 + l3**2} > ${l1**2} : ${t2}`)
    console.log(`${s1}^2 + ${a}^2 > ${s2}^2 : ${t3}`)
    console.log(`${l1}^2 + ${l3}^2 > ${l2}^2 : ${t3}`)
    console.log(`${l1**2 + l3**2} > ${l2**2} : ${t3}`)
    console.log("     - - - - -")
    if(!t1){
        _a = a
    } else if(!t2){
        _a = s1
    } else if(!t3){
        _a = s2
    } else {
        _a = a
    }
    if(a === _a){
        console.log("a == _a - initial obtusangle")
        let sides_order = [l2, l1]
        _second = get_arcsin(a3,sides_order[0],sides_order[1])
        if(isNaN(_second)) {
            sides_order.reverse()
            _second = get_arcsin(a3,sides_order[0],sides_order[1])
            l1 = sides_order[1]
        }
        console.log({_second})
        // ----------
        SIDES.steps = [s1, a, l1, SIDES[a]["side"], SIDES[a]["angle"] * pi / 180]
        console.log({s1, s2, a, l1, l3, a3})
        SIDES[s1]["angle"] = _second
        SIDES_TEX[s1]["angle"] = ROUND(_second*180/pi)
        _s = s1
        FIGURES.push(step_figure_svg(SIDES_TEX))
        // ----------
        _last = pi - _second - a3
        console.log({_last})
        SIDES[s2]["angle"] = _last
        SIDES_TEX[s2]["angle"] = ROUND(_last*180/pi)
        SIDES.last = [s2, s1, a, SIDES[s2]["angle"], SIDES[s1]["angle"], a3]
        _a = s2
        FIGURES.push(step_figure_svg(SIDES_TEX))
        // ----------
        SIDES.exclude = _a
    } else {
        console.log(`${_a} angle is obtuse`)
        _sides.splice(_sides.indexOf(_a),1)
        _sides.splice(_sides.indexOf(a),1)
        console.log({_a,a})
        _s = _sides[0]
        console.log({_s})
        _second = get_arcsin(a3,SIDES[a]["side"],SIDES[_s]["side"])
        console.log(`_second: ${_second*180/pi}`)
        SIDES[_s]["angle"] = _second
        SIDES_TEX[_s]["angle"] = ROUND(_second*180/pi)
        FIGURES.push(step_figure_svg(SIDES_TEX))
        console.log("Step 3 --------------")
        _last = pi - _second - a3
        console.log({_s, _a, a})
        SIDES_TEX[_a]["angle"] = ROUND(_last*180/pi)
        FIGURES.push(step_figure_svg(SIDES_TEX))
        console.log(`_last: ${_last*180/pi}`)
        console.log({a,_s,_a})
        SIDES.steps = [_s,a, SIDES[_s]["side"], SIDES[a]["side"], a3]
        SIDES.exclude = _a
        SIDES.last = [_a,_s,a,SIDES[_a]["angle"],SIDES[_s]["angle"],SIDES[a]["angle"]*pi/180]
    }

    console.log("<==<==<==<==<==<==<==<==")

    SIDES[a]["side"] = Number(SIDES[a]["side"])
    SIDES[a]["angle"] = a3
    SIDES[_a]["angle"] = _last
    SIDES[_s]["angle"] = _second
    SIDES.tex = SOL_SAS_TEX(SIDES, s1, s2, a, l3, SIDES[s1]["side"], SIDES[s2]["side"])
    SIDES.tex.push(FIGURES)
    console.log(SIDES)
    return [set_results(SIDES)]
}

const SOL_SSA = (os, swa, ms, SIDES, SIDES_TEX, ERRORS) => {
    let
        a3 = SIDES[swa]["angle"] * pi / 180,
        c = SIDES[os]["side"],
        b = SIDES[swa]["side"],
        D = (c/b) * Math.sin(a3),
        SIDES_TEX2 = JSON.parse(JSON.stringify(SIDES_TEX))

    console.log(">>--->> SIDES_TEX2")
    console.table(SIDES_TEX2)

    console.log(D)
    if(D > 1){
        console.log("D > 1")
        ERRORS.push("Can't create a triangle with that data")
        return [undefined]
    } else if (D <= 1) {
        let FIGURES = []
        FIGURES.push(step_figure_svg(SIDES_TEX))

        let y = Math.asin(D)
        SIDES_TEX[os]["angle"] = ROUND(y * 180 / pi)
        FIGURES.push(step_figure_svg(SIDES_TEX))

        let last_angle = pi - a3 - y
        SIDES_TEX[ms]["angle"] = ROUND(last_angle * 180 / pi)
        FIGURES.push(step_figure_svg(SIDES_TEX))

        let a = b * Math.sin(last_angle) / Math.sin(a3)
        SIDES_TEX[ms]["side"] = a

        SIDES[os]["angle"] = y
        SIDES[ms]["angle"] = last_angle
        SIDES[ms]["side"] = a
        SIDES[swa]["angle"] = a3

        SIDES.tex = SOL_SSA_TEX(a3, c, b, y, last_angle, ms, os, swa, a)
        SIDES.tex.push(FIGURES)
        // console.log("-=-=-=-=-=-=-=-=-=-=-=-")
        let RESULTS = set_results(SIDES)
        if(b < c){
            let FIGURES2 = []
            // -------------------------------------
            console.log("b < c")
            console.table(SIDES_TEX2)
            FIGURES2.push(step_figure_svg(SIDES_TEX2))

            let y2 = Math.PI - y
            SIDES_TEX2[os]["angle"] = ROUND(y2 * 180 / pi)
            FIGURES2.push(step_figure_svg(SIDES_TEX2))

            last_angle = pi - a3 - y2
            SIDES_TEX2[ms]["angle"] = ROUND(last_angle * 180 / pi)
            FIGURES2.push(step_figure_svg(SIDES_TEX2))

            a = b * Math.sin(last_angle) / Math.sin(a3)
            SIDES[os]["angle"] = y2
            SIDES[ms]["angle"] = last_angle
            SIDES[ms]["side"] = a
            // faltante, os, swa, ROUND(last_angle*180/pi), ROUND(y2*180/pi), ROUND(a3*180/pi)
            SIDES.tex2 = SOL_SSA_TEX2(a3, c, b, y2, last_angle, ms, os, swa, a, y)
            SIDES.tex2.push(FIGURES2)

            console.table(SIDES_TEX2)

            let RESULTS_2 = set_results(SIDES, "tex2")
            return [RESULTS,RESULTS_2]
        }
        return [RESULTS]
    }
}

const SOL_ASA = (a1, a2, s, SIDES, SIDES_TEX) => {
    let
        FIGURES = [],
        ang1 = SIDES[a1]["angle"] * pi / 180,
        ang2 = SIDES[a2]["angle"] * pi / 180,
        side3 = SIDES[s]["side"]

    FIGURES.push(step_figure_svg(SIDES_TEX))

    let ang3 = pi - ang1 - ang2
    SIDES_TEX[s]["angle"] = ROUND(ang3 * 180 / pi)
    FIGURES.push(step_figure_svg(SIDES_TEX))

    let side1 = side3 * Math.sin(ang1) / Math.sin(ang3)
    SIDES_TEX[a1]["side"] = ROUND(side1)
    FIGURES.push(step_figure_svg(SIDES_TEX))

    let side2 = side3 * Math.sin(ang2) / Math.sin(ang3)
    SIDES_TEX[a2]["side"] = format_value(side2.toFixed(2))

    SIDES[a1]["side"] = side1
    SIDES[a2]["side"] = side2
    SIDES[a1]["angle"] = ang1
    SIDES[a2]["angle"] = ang2
    SIDES[s]["angle"] = ang3
    SIDES.tex = SOL_ASA_TEX(a1, a2, s, ang1, ang2, side3, SIDES)
    SIDES.tex.push(FIGURES)
    return [set_results(SIDES)]
}

const SOL_AAS = (a1, a2, ms, SIDES, SIDES_TEX) => {
    let
        FIGURES = [],
        ang2 = SIDES[a1]["angle"] * pi / 180,
        ang1 = SIDES[a2]["angle"] * pi / 180,
        side1 = SIDES[a2]["side"]
    FIGURES.push(step_figure_svg(SIDES_TEX))

    let ang3 = pi - ang1 - ang2
    SIDES_TEX[ms]["angle"] = ROUND(ang3 * 180 / pi)
    FIGURES.push(step_figure_svg(SIDES_TEX))

    let side2 = side1 * Math.sin(ang2) / Math.sin(ang1)
    SIDES_TEX[a1]["side"] = ROUND(side2)
    FIGURES.push(step_figure_svg(SIDES_TEX))

    let side3 = side1 * Math.sin(ang3) / Math.sin(ang1)
    SIDES_TEX[ms]["side"] = ROUND(side3)

    SIDES[a2]["angle"] = side1
    SIDES[a1]["side"] = side2
    SIDES[ms]["side"] = side3
    SIDES[a2]["angle"] = ang1
    SIDES[a1]["angle"] = ang2
    SIDES[ms]["angle"] = ang3
    SIDES.tex = SOL_AAS_TEX(a1, a2, ms, ang3, ang1, ang2, SIDES)
    SIDES.tex.push(FIGURES)
    return [set_results(SIDES)]
}
