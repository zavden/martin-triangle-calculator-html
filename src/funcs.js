const print = console.log

const get_arc_cos = (a, b, c, ERROR) => {
    let r = Math.acos((b**2+c**2-a**2)/(2*b*c))
    if(isNaN(r)) {
        ERROR.push("The sum of two sides must be larger than the third")
    }
    return r
}

const get_side_from_2_sides_and_angle = (s1, s2, angle) => {
    return Math.sqrt(s1**2 + s2**2 - (2*s1*s2*Math.cos(angle)))
}

const get_arcsin = (gamma, c, a) => {
    console.log(`Math.sin(gamma) * a/c = ${Math.sin(gamma) * a/c}`)
    return Math.asin(Math.sin(gamma) * a/c)
}

const set_results = (SIDES, tex="tex") => {
    let result = {
        a : {
            side: SIDES.a.side.toFixed(2),
            angle: (SIDES.a.angle * 180 / Math.PI).toFixed(2),
        },
        b : {
            side: SIDES.b.side.toFixed(2),
            angle: (SIDES.b.angle * 180 / Math.PI).toFixed(2),
        },
        c : {
            side: SIDES.c.side.toFixed(2),
            angle: (SIDES.c.angle * 180 / Math.PI).toFixed(2),
        }
    }
    if(SIDES.hasOwnProperty(tex)){
        result[tex] = [...SIDES[tex]]
    }
    return result
}

const format_value = v => String(Number(v)).replace(".",",")

const fv = v => String(Number(v).toFixed(2)).replace(".",",")


const convert_results_to_html = (RESULTS) => {
    return `
        <p class="p-result">\\(a=\\) ${format_value(RESULTS.a.side)}</p>
        <p class="p-result">\\(b=\\) ${format_value(RESULTS.b.side)}</p>
        <p class="p-result">\\(c=\\) ${format_value(RESULTS.c.side)}</p>
        <p class="p-result">\\(\\alpha=\\) ${format_value(RESULTS.a.angle)}°</p>
        <p class="p-result">\\(\\beta=\\) ${format_value(RESULTS.b.angle)}°</p>
        <p class="p-result">\\(\\gamma=\\) ${format_value(RESULTS.c.angle)}°</p>
    `
}

const get_tex_to_svg = (x,y,tex,g_class="svg-text", font_size= "2em") => {
    return `
    <foreignObject x="${x}" y="${y}" width="150" height="80" text-anchor="middle" class="${g_class}">
       <body xmlns="http://www.w3.org/1999/xhtml" style="color: red">
         <div style="font-size: ${font_size};fill-opacity: 1">
           \\(${tex}\\)
         </div>
       </body>
     </foreignObject>
    `
}

const get_symbol = n => {
    if(n==="a") return "\\alpha"
    else if(n==="b") return "\\beta"
    else if(n==="c") return "\\gamma"
    else {
        console.log(n)
        return `EGS: ${n}`
    }
}


const get_symbol_prime = symbol => {
        if(symbol==="a")
            return "\\alpha'"
        else if(symbol==="b")
            return "\\beta'"
        else if(symbol==="c")
            return "\\gamma'"
        else
            return "ERROR EN get_symbol_prime"
}

const modify_results_tex = (RESULTS, tex, step_solutions) => {
    if(RESULTS.hasOwnProperty(tex)){
        print("===== HAS TEX ========")
        // step_solutions.innerHTML += "\\begin{align*}"
        RESULTS[tex][0].forEach((e,i) => {
            // console.log(e)
            let step_container = document.createElement("div")
            step_container.setAttribute("class","single-step")
            // FIGURES
            let tex_figure = document.createElement("div")
            let tex_figure_div = document.createElement("div")
            tex_figure_div.innerHTML = RESULTS[tex][2][i]
            tex_figure_div.setAttribute("class","tex-figure-div")
            tex_figure.appendChild(tex_figure_div)
            // TEX STEP
            let tex_step = document.createElement("div")
            tex_step.appendChild(
                document.createTextNode(`
                                            \\begin{align*}
                                            ${e}
                                            \\end{align*}
                                        `)
            )
            tex_step.setAttribute("class", "blue")
            // VALUES
            let values =  document.createElement("div")
            let value_content = document.createElement("div")
            values.setAttribute("class", "temp-values")
            value_content.setAttribute("class", "value-content")
            value_content.appendChild(
                document.createTextNode(`
                                            ${RESULTS[tex][1][i]}
                                        `)
            )
            value_content.setAttribute("class", "dark-green")
            values.appendChild(value_content)
            // APPEND VALUES
            step_container.appendChild(tex_figure)
            step_container.appendChild(tex_step)
            step_container.appendChild(values)
            step_solutions.appendChild(step_container)
        })
    } else {
        print("===== NO HAS TEX ========")
    }
}

const title_option = text => {
    let t = document.createElement("div")
    t.setAttribute("class","title-option")
    t.appendChild(
        document.createTextNode(text)
    )
    return t
}

const new_get_svg_from_sides = (RESULTS) => {
    console.log("INIT SVG >>>>>>>>>>>>")
    let vA = Number(RESULTS.a.side),
        vB = Number(RESULTS.b.side),
        vC = Number(RESULTS.c.side),
        vAlpha = Number(RESULTS.a.angle),
        vBeta = Number(RESULTS.b.angle),
        vGamma = Number(RESULTS.c.angle),
        max_val = Number(Math.max(vA,vB,vC)),
        mS = 600

    console.log({vA,vB,vC})
    console.log({vAlpha,vBeta,vGamma})
    console.log(`MAX_VAL = ${max_val}`)
    let W,H,A,B,C,last_dot_x,h,x_shift,y_shift,ratio,extra_right_padding,extra_x=0
    if(Number(max_val)===Number(vA)){
        console.log(`MAX_VAL = A = ${max_val} = ${vA}`)
        ratio = mS / vA
        A = mS
        B = vB * ratio
        C = vC * ratio
        extra_right_padding = 80
        last_dot_x = B * Math.cos(vGamma * pi / 180)
    } else if(Number(max_val)===Number(vB)){
        console.log(`MAX_VAL = B = ${max_val} = ${vB}`)
        if(vBeta > 90){
            B = mS / Math.cos(vGamma * pi / 180)
            ratio = B / vB
            console.log(`obtuse`)
        } else {
            ratio = mS / vB
            B = vB * ratio
            console.log(`acute`)
        }
        A = vA * ratio
        C = vC * ratio
        extra_right_padding = 200
        console.log({vA, vB, vC})
        console.log({ratio,mS,A,B,C})
        console.log({vAlpha,vBeta,vGamma})
        last_dot_x = B * Math.cos(vGamma * pi / 180)
    } else if(Number(max_val)===Number(vC)){
        console.log(`MAX_VAL = C = ${max_val} = ${vC}`)
        if(vGamma > 90){
            C = mS / Math.cos(vBeta * pi / 180)
            ratio = C / vC
            console.log(`obtuse`)
        } else {
            ratio = mS / vC
            C = vC * ratio
            console.log(`acute`)
        }
        A = vA * ratio
        B = vB * ratio
        extra_right_padding = 20
        if(vGamma > 90 && mS > A) {
            console.log("x-x-x-x-x")
            extra_x = mS - A
        }
        console.log({mS, C, A})
        last_dot_x = B * Math.cos(vGamma * pi / 180)
    }

    h = B * Math.sin(vGamma * pi / 180)
    x_shift = 140
    y_shift = 120
    W = A+x_shift
    H = h+y_shift



    return `
    <svg width="${W+extra_right_padding+extra_x}" height="${H}" class="svg-end">
       <g transform="translate(${(W-A)/2},${(H-h)/2})">
          ${get_tex_to_svg(extra_x+A/2-30, h,`\\mbox{${format_value(vA)}}`, "svg-end-font")}
          <g transform="translate(-120,-40)">
            ${get_tex_to_svg(extra_x+last_dot_x/2 + 40, h/2,`\\mbox{${format_value(vB)}}`, "svg-end-font")}
          </g>
          <g transform="translate(25,-40)">
            ${get_tex_to_svg(extra_x+A-(A-last_dot_x)/2, h/2,`\\mbox{${format_value(vC)}}`, "svg-end-font")}
          </g>
          <g transform="translate(-60,0)">
            ${get_tex_to_svg(extra_x, h,`\\mbox{${format_value(vGamma)}}^\\circ`, "svg-end-font")}
          </g>
          <g transform="translate(0,0)">
            ${get_tex_to_svg(extra_x+A, h,`\\mbox{${format_value(vBeta)}}^\\circ`, "svg-end-font")}
          </g>
          <g transform="translate(0,0)">
            ${get_tex_to_svg(extra_x+last_dot_x, -40,`\\mbox{${format_value(vAlpha)}}^\\circ`, "svg-end-font")}
          </g>
          <polygon 
                points="${extra_x},${h}  ${A+extra_x},${h} ${last_dot_x+extra_x},0"
                fill="none" stroke="black" stroke-width="5"
                stroke-linejoin="round"
          />
      </g>
     
    </svg>
    `
}