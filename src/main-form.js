let all_inputs = document.querySelectorAll(".in-value")
let a_v = document.getElementById("a_v")
let case_container = document.getElementById("case-container")
let step_solutions = document.getElementById("step-solutions")
let button  = document.getElementById("init-button")
a_v.focus()

window.MathJax = {
    loader: {
        load: [
            '[tex]/mathtools',
            '[tex]/html',
        ]
    },
    tex: {
        packages: {
            '[+]': [
                'mathtools',
                'html',
            ]
        }
    }
}



all_inputs_arr = Array.from(all_inputs)


all_inputs.forEach((e, i, arr) => {
    e.addEventListener("keyup", (event) => {
        if (event.defaultPrevented) {}
        if (event.key === "Enter") run_program()
    })
})

button.addEventListener("click",() => run_program())




const run_program = () => {
    console.clear()
    MathJax.typesetPromise()
    const raw_values = Array.from(all_inputs_arr,x => x.value)
    const introduced_values = raw_values.filter(x => x !== "")
    print(raw_values)
    print(introduced_values)
    let ERRORS = []
    if(introduced_values.length > 3) ERRORS.push("There is too much data")
    if(introduced_values.length < 3) ERRORS.push("There are not enough values")

    introduced_values.forEach(e => {
        if(!!return_error_value(e))
            ERRORS.push(return_error_value(e))
    })

    if(ERRORS.length > 0){
        let err_msj = ERRORS.join("\n")
        alert(err_msj)
    }
    else {
        //////// RUN CODE
        // console.clear()
        let SIDES = read_sides()
        print( JSON.stringify( SIDES ) )
        console.table( read_sides() )

        const main_dict = [SIDES.a.side, SIDES.b.side, SIDES.c.side]
        let nL = main_dict.filter(x => x !== false).length
        print(`nL = ${nL}`)

        // RUN CHECK CASES
        let MCR = main_check(nL, SIDES, ERRORS)
        let t_case = MCR[0]
        let RESULTS, RESULTS_1, RESULTS_2
        if(MCR.length === 2) {
            RESULTS = MCR[1]
        } else if(MCR.length === 3) {
            RESULTS_1 = MCR[1]
            RESULTS_2 = MCR[2]
        }
        // Clear p_result node
        if(ERRORS.length === 0){
            case_container.innerHTML = ""
            case_container.appendChild(document.createTextNode(`${t_case}`))

            // let results_container = document.createElement("div")
            if(MCR.length === 2){
                let results_html = document.createElement("div")
                let results_container = document.createElement("div")
                let svg_container = document.createElement("div")
                results_container.setAttribute("class","results-container")
                svg_container.setAttribute("class","svg-container")
                results_html.innerHTML = convert_results_to_html(RESULTS)
                svg_container.innerHTML = new_get_svg_from_sides(RESULTS)
                step_solutions.innerHTML = ""
                modify_results_tex(RESULTS, "tex", step_solutions)
                results_container.appendChild(svg_container)
                results_container.appendChild(results_html)
                step_solutions.appendChild(results_container)
            } else if (MCR.length === 3){ // <------- CASE SSA with D <= 1
                step_solutions.innerHTML = ""
                let
                    results_container1 = document.createElement("div"),
                    results_container2 = document.createElement("div"),
                    svg_container1 = document.createElement("div"),
                    svg_container2 = document.createElement("div"),
                    results_html1 = document.createElement("div"),
                    results_html2 = document.createElement("div")

                results_container1.setAttribute("class","results-container")
                results_container2.setAttribute("class","results-container")
                svg_container1.setAttribute("class","svg-container")
                svg_container2.setAttribute("class","svg-container")

                results_html1.innerHTML = convert_results_to_html(RESULTS_1)
                results_html2.innerHTML = convert_results_to_html(RESULTS_2)
                svg_container1.innerHTML = new_get_svg_from_sides(RESULTS_1)
                svg_container2.innerHTML = new_get_svg_from_sides(RESULTS_2)

                results_container1.appendChild(svg_container1)
                results_container1.appendChild(results_html1)

                results_container2.appendChild(svg_container2)
                results_container2.appendChild(results_html2)

                step_solutions.appendChild(document.createElement("br"))
                step_solutions.appendChild(title_option("Option 1: acute angle"))
                step_solutions.appendChild(document.createElement("br"))

                modify_results_tex(RESULTS_1, "tex", step_solutions)
                step_solutions.appendChild(results_container1)

                step_solutions.appendChild(document.createElement("br"))
                // step_solutions.appendChild(document.createElement("hr"))
                step_solutions.appendChild(document.createElement("br"))

                step_solutions.appendChild(title_option("Option 2: obtuse angle"))
                step_solutions.appendChild(document.createElement("br"))

                modify_results_tex(RESULTS_2, "tex2", step_solutions)
                step_solutions.appendChild(results_container2)
                // results_html.innerHTML = convert_results_2_to_html(RESULTS_1,RESULTS_2)
                console.table(RESULTS_1)
                console.log("<-<-<-<-<-<")
            }
            // p_result.appendChild(results_html)
            // MathJax.Hub.Queue(["Typeset", MathJax.Hub, svg_container]);
            MathJax.typesetPromise()
        } else {
            let ERRORS_SET = new Set(ERRORS)
            let err = []
            ERRORS_SET.forEach(e => err.push(e))
            let err_msj = err.join("\n")
            alert(err_msj)
        }
        console.table(RESULTS)
        console.log("------------------")
    }

}

const read_sides = () => {
    return {
        a : {
            side: get_value(all_inputs_arr[0].value),
            angle: get_value(all_inputs_arr[3].value),
        },
        b : {
            side: get_value(all_inputs_arr[1].value),
            angle: get_value(all_inputs_arr[4].value),
        },
        c : {
            side: get_value(all_inputs_arr[2].value),
            angle: get_value(all_inputs_arr[5].value),
        },
    }
}

const return_error_value = (val) => {
    if(isNaN(val)) return `${val} is not a number`
    else if(Number(val.value)<0) return `${val} cannot be negative`
    else return false
}

const get_value = (v) => {
    if(v === "")
        return false
    else
        return Number(v)
}
