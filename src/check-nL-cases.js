const main_check = (nL, SIDES, ERRORS) => {
    let t_case = ""
    let SIDES_TEX = {...SIDES}
    let RESULTS
    if(nL === 3){
        t_case = "CASE: SSS"
        RESULTS = SOL_SSS(SIDES, SIDES_TEX, ERRORS)
        // ------------------------------------------------------------------
    } else if(nL === 2) {
        console.log("LAL or SSA")
        // SIDES[order_sides[-1]]["angle"] is not False
        let true_sides = ["a","b","c"].filter(x => SIDES[x]["side"] !== false)
        let false_sides = ["a","b","c"].filter(x => SIDES[x]["side"] === false)
        let order_sides = [...true_sides,...false_sides]
        if (SIDES[order_sides[order_sides.length-1]]["angle"] !== false){
            t_case = "CASE: SAS" // HERE IT IS LAL FUNC
            console.table(order_sides)
            RESULTS = SOL_SAS(order_sides[0],order_sides[1],order_sides[2],SIDES,SIDES_TEX)
        // ------------------------------------------------------------------
        } else {
            t_case = "CASE: SSA" // HERE IT IS LLA FUNC
            let
                side_width_angle = true_sides.filter(x => SIDES[x]["angle"]),
                only_side = true_sides.filter(x => SIDES[x]["angle"] === false && SIDES[x]["side"] !== false)
            let actual_sides = [side_width_angle[0],only_side[0]]
            let all_sides = ["a","b","c"]
            let missing_side = all_sides
                .filter(w => !actual_sides.includes(w))
                .concat(actual_sides.filter(x => !all_sides.includes(x)))
            RESULTS = SOL_SSA(only_side[0], side_width_angle[0], missing_side[0], SIDES, SIDES_TEX, ERRORS)
        // ------------------------------------------------------------------
        }
    } else if(nL === 1) {
        console.log("AAS or ASA")
        let true_angles = ["a","b","c"].filter(x => SIDES[x]["angle"] !== false)
        let false_angles = ["a","b","c"].filter(x => SIDES[x]["angle"] === false)
        let order_angles = [...true_angles,...false_angles]
        if (SIDES[order_angles[order_angles.length-1]]["side"] !== false){
            t_case = "CASE: ASA" // HERE IT IS LAL ALA
            RESULTS = SOL_ASA(order_angles[0],order_angles[1],order_angles[2], SIDES, SIDES_TEX)
        // ------------------------------------------------------------------
        } else {
            t_case = "CASE: AAS" // HERE IT IS LLA AAL
            let
                side_width_angle = true_angles.filter(x => SIDES[x]["side"]),
                only_angle = true_angles.filter(x => SIDES[x]["side"] === false && SIDES[x]["angle"] !== false)
            let actual_sides = [side_width_angle[0],only_angle[0]]
            let all_sides = ["a","b","c"]
            let missing_side = all_sides
                .filter(w => !actual_sides.includes(w))
                .concat(actual_sides.filter(x => !all_sides.includes(x)))
            RESULTS = SOL_AAS(only_angle[0],side_width_angle[0], missing_side[0], SIDES, SIDES_TEX)
        // ------------------------------------------------------------------
        }
    } else if(nL === 0){
        t_case = "AAA cannot be solved"
        alert(t_case)
    } else {
        t_case = "ERROR IN main_check() func"
        alert(t_case)
    }
    console.log(t_case)
    if(RESULTS.length === 1) return [t_case, RESULTS[0]]
    if(RESULTS.length === 2) return [t_case, RESULTS[0], RESULTS[1]]
}