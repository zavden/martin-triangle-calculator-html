const SOL_SSS_TEX = (SIDES) => {
    let
        TEX_RESULTS,
        TEX_STEPS = [],
        l1 = SIDES.a.side,
        l2 = SIDES.b.side,
        l3 = SIDES.c.side
    arc_cos_tex(TEX_STEPS,
        SIDES.max_values.max.side,
        SIDES.max_values.middle.side,
        SIDES.max_values.minor.side,
        SIDES.max_values.max.angle,
        SIDES.max_values.max.letter,
        SIDES.max_values.middle.letter,
        SIDES.max_values.minor.letter,
    )
    arc_sin_angle_side(TEX_STEPS,
        SIDES.max_values.middle.letter,
        SIDES.max_values.max.letter,
        SIDES.max_values.middle.side,
        SIDES.max_values.max.side,
        SIDES.max_values.max.angle,
    )
    sum_of_angles(
        SIDES.max_values.minor.letter,
        SIDES.max_values.middle.letter,
        SIDES.max_values.max.letter,
        SIDES.max_values.minor.angle,
        SIDES.max_values.middle.angle,
        SIDES.max_values.max.angle,
        TEX_STEPS
    )

    TEX_RESULTS = [
        `
        \\begin{aligned}
        & a = ${l1} \\\\
        & b = ${l2} \\\\
        & c = ${l3} \\\\
        & ${get_symbol(SIDES.max_values.max.letter)} = ${fv(SIDES.max_values.max.angle*180/pi)}^\\circ
        \\end{aligned}
        `,
        `
        \\begin{aligned}
        ${get_symbol(SIDES.max_values.middle.letter)} = ${fv(SIDES.max_values.middle.angle*180/pi)}^\\circ
        \\end{aligned}
        `,
        `
        \\begin{aligned}
        ${get_symbol(SIDES.max_values.minor.letter)} = ${fv(SIDES.max_values.minor.angle*180/pi)}^\\circ
        \\end{aligned}
        `,
    ]
    return [TEX_STEPS,TEX_RESULTS]
}

const SOL_SAS_TEX = (SIDES, s1, s2, a, l3, a1, a2) => {
    let
        TEX_RESULTS,
        TEX_STEPS = [],
        l1 = SIDES[s1]["side"],
        l2 = SIDES[s2]["side"],
        a3 = SIDES[a]["angle"]

    arc_cos_sqrt_tex(TEX_STEPS,s1,s2,a,l1,l2,a3,l3)
    arc_sin_angle_side(TEX_STEPS,...SIDES.steps)
    sum_of_angles(...SIDES.last,TEX_STEPS)

    TEX_RESULTS = [
        `
        \\begin{aligned}
        & ${s1} = ${l1} \\\\
        & ${s2} = ${l2} \\\\
        & ${get_symbol(a)} = ${format_value(Number(a3*180/pi).toFixed(2))}^\\circ \\\\
        & ${a} = ${fv(l3)}
        \\end{aligned}
        `,
        `
        \\begin{aligned}
        ${get_symbol(SIDES.steps[0])} = ${fv(SIDES[SIDES.steps[0]]["angle"]*180/pi)}^\\circ
        \\end{aligned}
        `,
        `
        \\begin{aligned}
        ${get_symbol(SIDES.exclude)} = ${fv(SIDES[SIDES.exclude]["angle"]*180/pi)}^\\circ
        \\end{aligned}
        `,
    ]
    return [TEX_STEPS,TEX_RESULTS]
}

const SOL_ASA_TEX = (a1, a2, s, ang1, ang2, side3, SIDES) => {
    let
        TEX_RESULTS,
        TEX = [],
        ang3 = SIDES[s]["angle"],
        side1 = SIDES[a1]["side"],
        side2 = SIDES[a2]["side"]

    sum_of_angles(s, a2, a1, ang3, ang2, ang1, TEX)
    arc_sin_side_angle(TEX, a1, s, side1, ang1*180/pi, side3, ang3*180/pi)
    arc_sin_side_angle(TEX, a2, s, side2, ang2*180/pi, side3, ang3*180/pi)

    TEX_RESULTS = [
        `
        \\begin{align*}
        & ${s} = ${side3} \\\\ 
        & ${get_symbol(a1)} = ${format_value(ROUND(ang1 * 180 / pi))}^\\circ \\\\ 
        & ${get_symbol(a2)} = ${format_value(ROUND(ang2 * 180 / pi))}^\\circ \\\\ 
        & ${get_symbol(s)} = ${format_value(ROUND(ang3 * 180 / pi))}^\\circ 
        \\end{align*}
        `,
        `\\(${a1} = ${format_value(ROUND(side1))}\\)`,
        `\\(${a2} = ${format_value(ROUND(side2))}\\)`
    ]
    return [TEX,TEX_RESULTS]
}

const SOL_AAS_TEX = (a1, a2, ms, ang3, ang1, ang2, SIDES) => {
    let TEX = [],TEX_RESULTS,
        side1 = SIDES[a2]["side"],
        side2 = SIDES[a1]["side"],
        side3 = SIDES[ms]["side"]

    sum_of_angles(ms, a2, a1, ang3, ang1, ang2, TEX)
    arc_sin_side_angle(TEX, a1, a2, side2, ang2 * 180 / pi, side1, ang1 * 180 / pi)
    arc_sin_side_angle(TEX, ms, a2, side3, ang3 * 180 / pi, side1, ang1 * 180 / pi)

    let s_order = [a1,a2].sort()

    TEX_RESULTS = [
        `
        \\begin{align*}
        & ${a2} = ${side1} \\\\ 
        & ${get_symbol(s_order[0])} = ${format_value(ROUND(SIDES[s_order[0]]["angle"] * 180 / pi))}^\\circ \\\\ 
        & ${get_symbol(s_order[1])} = ${format_value(ROUND(SIDES[s_order[1]]["angle"] * 180 / pi))}^\\circ \\\\ 
        & ${get_symbol(ms)} = ${format_value(ROUND(ang3 * 180 / pi))}^\\circ 
        \\end{align*}
        `,
        `\\(${a1} = ${format_value(ROUND(side2))} \\)`,
        `\\(${ms} = ${format_value(ROUND(side3))} \\)`
    ]

    return [TEX,TEX_RESULTS]
}

const SOL_SSA_TEX = (a3, c, b, y1, last_angle, ms, os, swa, a) => {
    let TEX = []

    arc_sin_angle_side(TEX, os, swa, c, b, a3)
    sum_of_angles(ms, os, swa, last_angle, y1, a3, TEX)
    arc_sin_side_angle(TEX, ms, swa, a, last_angle * 180 / pi, b, a3 * 180 / pi)

    let TEX_RESULTS = [
        `
        \\begin{align*}
        & ${os} = \\mbox{${c}} \\\\ 
        & ${swa} = \\mbox{${b}} \\\\ 
        & ${get_symbol(swa)} = \\mbox{${format_value(ROUND(a3 * 180 / pi))}}^\\circ \\\\ 
        & ${get_symbol(os)} = \\mbox{${format_value(ROUND(y1 * 180 / pi))}}^\\circ 
        \\end{align*}
        `,
        `
        \\(${get_symbol(ms)} = \\mbox{${format_value(ROUND(last_angle*180/pi))}}^\\circ \\)
        `,
        `
        \\(${ms} = \\mbox{${format_value(ROUND(a))}}\\)
        `
    ]

    return [TEX,TEX_RESULTS]
}

const SOL_SSA_TEX2 = (a3, c, b, y1, last_angle, ms, os, swa, a, yp) => {
    let TEX = []

    show_optusangle(TEX, os, yp)
    sum_of_angles_prime(ms, os, swa, ROUND(last_angle*180/pi), ROUND(y1*180/pi), ROUND(a3*180/pi), TEX)
    arc_sin_side_angle(TEX, ms, swa, a, last_angle * 180 / pi, b, a3 * 180 / pi)

    let TEX_RESULTS = [
        `
        \\begin{align*}
        & ${os} = \\mbox{${c}} \\\\ 
        & ${swa} = \\mbox{${b}} \\\\ 
        & ${get_symbol(swa)} = \\mbox{${format_value(ROUND(a3 * 180 / pi))}}^\\circ \\\\ 
        & ${get_symbol(os)}' = \\mbox{${format_value(ROUND(y1 * 180 / pi))}}^\\circ 
        \\end{align*}
        `,
        `
        \\(${get_symbol(ms)} = \\mbox{${format_value(ROUND(last_angle*180/pi))}}^\\circ \\)
        `,
        `
        \\(${ms} = \\mbox{${format_value(ROUND(a))}}\\)
        `
    ]

    return [TEX,TEX_RESULTS]
}