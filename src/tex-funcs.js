const arc_cos_sqrt_tex = (TEX,s1,s2,a,l1,l2,a3,l3) => {
    let a_symbol = get_symbol(a)
    l1 = format_value(Number(l1))
    l2 = format_value(Number(l2))
    l3 = format_value(Number(l3).toFixed(2))
    a3 = format_value(Number(Number(a3 * 180 / pi).toFixed(2)))

    TEX.push(`
        & ${a}^2 = \\ \\ \\ \\centerto{${s1}^2}{${l1}^2} + \\centerto{${s2}^2}{${l2}^2} - 2 \\phantom{-}\\centerto{${s1}}{${l1}}\\phantom{-}\\! \\centerto{${s2}}{${l2}} \\cdot\\cos \\centerto{${a_symbol}}{${a3}}\\\\
        & ${a} \\ = \\sqrt{\\centerto{${s1}^2}{${l1}^2} + \\centerto{${s2}^2}{${l2}^2} - 2 \\phantom{-}\\centerto{${s1}}{${l1}}\\phantom{-}\\! \\centerto{${s2}}{${l2}} \\cdot\\cos \\centerto{${a_symbol}}{${a3}}}\\\\
        & ${a} \\ = \\sqrt{${l1}^2 + ${l2}^2 - 2\\cdot ${l1}\\cdot ${l2} \\cdot \\cos ${a3}^\\circ}\\\\
        & ${a} \\ = ${l3}
    `)
}

const arc_cos_tex = (TEX,l1,l2,l3,a1,f1,f2,f3) => {
    let angle = get_symbol(f1)
    let sides = [
        [l2,f2],
        [l3,f3]
    ]
    let sides_sort = sides.sort(function(a, b) {
        return b[1] - a[1];
    });
    l2 = sides_sort[0][0]
    l3 = sides_sort[1][0]
    f2 = sides_sort[0][1]
    f3 = sides_sort[1][1]
    TEX.push(`
        &\\phantom{cos^{-1}\\Big(\\ \\,\\,} \\centerto{${f1}^2 }{ ${l1}^2 } \\phantom{\\centerto{ ${f2} }{ ${l2} } - \\centerto{ ${f3} }{ ${l3} } =\\Big)ab}= ${f2}^2 + ${f3}^2 - 2${f2}${f3} \\cdot\\cos ${angle} \\\\
        &\\phantom{cos^{-1}\\Big(\\ \\,\\,} \\centerto{ ${f1}^2}{ ${l1}^2 } - \\centerto{ ${f2}^2 }{ ${l2}^2} - \\centerto{ ${f3}^2}{ ${l3}^2} \\phantom{\\Big)\\,\\,} = \\phantom{ ${f2}^2 + ${f3}^2} - 2 ${f2} ${f3} \\cdot\\cos ${angle}\\\\
        &\\phantom{cos^{-1}\\Big(\\ \\,} \\frac{\\centerto{${f1}^2}{${l1}^2} - \\centerto{${f2}^2}{${l2}^2} - \\centerto{${f3}^2}{${l3}^2}}{- 2${f2}${f3}} \\phantom{\\Big)} = \\phantom{${f2}^2 + ${f3}^2 - 2${f2}${f3} \\cdot\\,} \\cos ${angle}\\\\
        &\\cos^{-1}\\left(\\frac{\\centerto{${f1}^2}{${l1}^2} - \\centerto{${f2}^2}{${l2}^2} - \\centerto{${f3}^2}{${l3}^2}}{- 2${f2}${f3}}\\right) = \\phantom{${f2}^2 + ${f3}^2 - 2${f2}${f3} \\cdot\\cos\\,} ${angle}\\\\
        &\\cos^{-1}\\left(\\frac{${l1}^2 - ${l2}^2 - ${l3}^2}{- 2\\cdot ${l2}\\cdot ${l3}}\\right) = \\phantom{${f2}^2 + ${f3}^2 - 2${f2}${f3} \\cdot\\cos\\,} ${angle}\\\\
    `)
}

const sum_of_angles = (ms, s2, s1, a3, a2, a1,TEX, opt=false) => {
    let
        s1_s = get_symbol(s1),
        s2_s = get_symbol(s2),
        a_s = get_symbol(ms)
    let t1 = `& \\alpha + \\beta + \\gamma = 180^\\circ \\\\`
    let sides = [
        [s1, s1_s, a1],
        [s2, s2_s, a2],
    ]
    let sides_sort = sides.sort();
    let
        v1 = sides_sort[0],
        v2 = sides_sort[1]

    s1_s = v1[1]
    a1 = format_value((v1[2] * 180 / pi).toFixed(2))

    s2_s = v2[1]
    a2 = format_value((v2[2] * 180 / pi).toFixed(2))
    let t2


    if(a_s === `\\alpha`){
        t2 = `
            & ${a_s} \\phantom{+\\beta+\\gamma\\ \\,}= 180^\\circ - \\centerto{${s1_s}}{${a1}^\\circ} - \\centerto{${s2_s}}{${a2}^\\circ} \\\\
            & ${a_s} \\phantom{+\\beta+\\gamma\\ \\,}= 180^\\circ - ${a1}^\\circ - ${a2}^\\circ \\\\
        `
    } else if(a_s === `\\beta`) {
        t2 = `
            & \\phantom{\\alpha +}\\,\\, ${a_s} \\phantom{\\alpha +}\\,\\, = 180^\\circ - \\centerto{${s1_s}}{${a1}^\\circ} - \\centerto{${s2_s}}{${a2}^\\circ} \\\\
            & \\phantom{\\alpha +}\\,\\, ${a_s} \\phantom{\\alpha +}\\,\\, = 180^\\circ - ${a1}^\\circ - ${a2}^\\circ \\\\
        `
    } else if(a_s === `\\gamma`) {
        t2 = `
            & \\phantom{\\alpha + --\\,} ${a_s} = 180^\\circ - \\centerto{${s1_s}}{${a1}^\\circ} - \\centerto{${s2_s}}{${a2}^\\circ} \\\\
            & \\phantom{\\alpha + --\\,} ${a_s} = 180^\\circ - ${a1}^\\circ - ${a2}^\\circ \\\\
        `
    }

    TEX.push(`
        ${t1}
        ${t2}
    `)

}

const arc_sin_side_angle = (TEX, a, b, a_n, alpha, b_n, beta) => {
    let
        a_s = get_symbol(a),
        b_s = get_symbol(b)
    b_n = format_value(b_n.toFixed(2))
    beta = format_value(beta.toFixed(2))
    alpha = format_value(alpha.toFixed(2))
    a_n = format_value(a_n.toFixed(2))
    TEX.push(`
        & \\frac{${a}}{\\sin ${a_s}}=\\frac{${b}}{\\sin\\centerto{${b_s}}{(${beta}^\\circ)}} \\\\
        & \\phantom{-\\,}${a}\\phantom{-}=\\frac{${b}}{\\sin\\centerto{${b_s}}{(${beta}^\\circ)}}\\cdot \\sin\\centerto{${a_s}}{${alpha}^\\circ)} \\\\
        & \\phantom{-\\,}${a}\\phantom{-}=\\frac{${b_n}}{\\sin(${beta}^\\circ)}\\cdot \\sin (${alpha}^\\circ) \\\\
        & \\phantom{-\\,}${a}\\phantom{-}=${a_n}
    `)
}

const arc_sin_angle_side = (TEX, os, swa, c, b, a3, y) => {
    let
        os_s = get_symbol(os),
        swa_s = get_symbol(swa),
        db = format_value(ROUND(a3 * 180 / pi))
    c = format_value(ROUND(c))
    b = format_value(ROUND(b))

    TEX.push(`
        & \\frac{\\sin ${os_s}}{${os}} = \\phantom{sin^{-1}\\Big(\\,} \\frac{\\sin\\centerto{${swa_s}}{(${db}^\\circ)}}{${swa}} \\\\
        & \\,\\,\\sin ${os_s} = \\phantom{sin^{-1}\\Big(\\,} \\frac{\\sin\\centerto{${swa_s}}{(${db}^\\circ)}}{${swa}} \\cdot \\centerto{${os}}{${c}} \\\\
        & \\,\\phantom{sin\\,} ${os_s} = \\sin^{-1}\\left(\\frac{\\sin\\centerto{${swa_s}}{(${db}^\\circ)}}{${swa}} \\cdot \\centerto{${os}}{${c}}\\right) \\\\
        & \\,\\phantom{sin\\,} ${os_s} = \\sin^{-1}\\left(\\frac{\\sin(${db}^\\circ)}{${b}} \\cdot ${c} \\right)
    `)
}

const show_optusangle = (TEX, os, y1) => {
    let
        a_s = get_symbol(os)

    y1 = format_value(ROUND(y1*180 / pi))

    TEX.push(`
        & ${a_s}' = 180^\\circ - \\centerto{${a_s}}{${y1}^\\circ} \\\\
        & ${a_s}' = 180^\\circ - ${y1}^\\circ
    `)
}

const sum_of_angles_prime = (ms, s2, s1, a3, a2, a1, TEX) => {
    console.log(`.... a_s => ${ms}`)
    let
        s1_s = get_symbol(s1),
        s2_s = get_symbol_prime(s2),
        a_s = get_symbol(ms)

    let values = [
        [s1 ,s1_s, a1],
        [s2 ,s2_s, a2],
    ]
    let values2 = [
        [s1 ,s1_s, a1],
        [s2 ,s2_s, a2],
        [ms ,a_s, a3],
    ]
    let sort_orders = values.sort()
    let sort_orders2 = values2.sort()
    let
        m1 = sort_orders2[0],
        m2 = sort_orders2[1],
        m3 = sort_orders2[2],
        v1 = sort_orders[0],
        v2 = sort_orders[1]

    s1_s = v1[1]
    a1 = v1[2]

    s2 = v2[0]
    s2_s = v2[1]
    a2 = v2[2]

    let
        t = `& ${m1[1]} + ${m2[1]} + ${m3[1]}= 180^\\circ \\\\`,
        r1 = [a_s, s1_s,format_value(a1),s2_s,format_value(a2)],
        r2 = [a_s, format_value(a1), format_value(a2)]
    if (a_s === "\\alpha") {
        t += `
        & ${r1[0]} \\phantom{+\\beta+\\gamma\\ \\,\\,\\,}= 180^\\circ - \\centerto{${r1[1]}}{${r1[2]}^\\circ} - \\centerto{${r1[3]}}{${r1[4]}^\\circ} \\\\
        & ${r2[0]} \\phantom{+\\beta+\\gamma\\ \\,\\,\\,}= 180^\\circ - ${r2[1]}^\\circ - ${r2[2]}^\\circ
        `
    } else if (a_s === "\\beta") {
        if (s2 === "c") {
            t += `
            & \\phantom{\\alpha +\\,}\\, ${r1[0]} \\phantom{\\alpha +\\,}= 180^\\circ - \\centerto{${r1[1]}}{${r1[2]}^\\circ} - \\centerto{${r1[3]}}{${r1[4]}^\\circ} \\\\
            & \\phantom{\\alpha +\\,}\\, ${r2[0]} \\phantom{\\alpha +\\,}= 180^\\circ - ${r2[1]}^\\circ - ${r2[2]}^\\circ
            `
        } else {
            t += `
            & \\phantom{\\alpha +\\,\\,} ${r1[0]} \\phantom{\\alpha +\\ }= 180^\\circ - \\centerto{${r1[1]}}{${r1[2]}^\\circ} - \\centerto{${r1[3]}}{${r1[4]}^\\circ} \\\\
            & \\phantom{\\alpha +\\,\\,} ${r2[0]} \\phantom{\\alpha +\\ }= 180^\\circ - ${r2[1]}^\\circ - ${r2[2]}^\\circ
            `
        }
    } else if (a_s === "\\gamma") {
        t += `
        & \\phantom{\\alpha + ${s2_s} +\\,\\,} ${r1[0]} = 180^\\circ - \\centerto{${r1[1]}}{${r1[2]}^\\circ} - \\centerto{${r1[3]}}{${r1[4]}^\\circ} \\\\
        & \\phantom{\\alpha + ${s2_s} +\\,\\,} ${r2[0]} = 180^\\circ - ${r2[1]}^\\circ - ${r2[2]}^\\circ
        `
    }
    TEX.push(t)
}


const tg = (l,t,sides) =>{
    if(sides[l][t]===false) return ""
    if(t==="angle") return `\\mbox{${format_value(sides[l][t])}}^\\circ`
    else if(t==="side") return `\\mbox{${format_value(sides[l][t])}}`
}

const step_figure_svg = sides => {
    let
        x_shift = 200,
        y_shift = 120,
        A = 300,
        h = 400,
        last_dot_x = h/4
    let
        W = A+x_shift,
        H = h+y_shift

    return `
    <svg width="${W}" height="${H}" class="svg-result">
          <g transform="translate(${(W-A)/2},${(H-h)/2})">
          <!-- A value--->
          ${get_tex_to_svg(A/2-30, h,tg("a","side",sides),font_size="3em")}
          <!-- B value--->
          <g transform="translate(-60,-40)">
            ${get_tex_to_svg(last_dot_x/2, h/2,tg("b","side",sides),font_size="3em")}
          </g>
          <!-- C value--->
          <g transform="translate(25,-40)">
            ${get_tex_to_svg(A-(A-last_dot_x)/2, h/2,tg("c","side",sides),font_size="3em")}
          </g>
          <!-- alpha value--->
          <g transform="translate(-20,170)">
            ${get_tex_to_svg(last_dot_x, -55,tg("a","angle",sides),font_size="3em")}
          </g>
          <!-- beta value--->
          <g transform="translate(-115,-40)">
            ${get_tex_to_svg(A, h,tg("b","angle",sides),font_size="3em")}
          </g>
          <!-- gamma value--->
          <g transform="translate(40,-40)">
            ${get_tex_to_svg(0, h,tg("c","angle",sides),font_size="3em")}
          </g>
          <polygon 
                points="0,${h}  ${A},${h} ${last_dot_x},0"
                fill="none" stroke="black" stroke-width="5"
                stroke-linejoin="round"
          />
      </g>
      <rect width="${W}" height="${H}" style="stroke-width:0;stroke:rgb(0,0,0)" fill="none" />
    </svg>
    `
}